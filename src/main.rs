//! The Automatic Catastrophe Machine (ACM) is a Discord
//! bot that runs arbitrary commands as root in a docker
//! image.
//!
//! Yes, this is a terrible idea.
//!
//! No, I don't care.

use log::info;

use crossbeam_channel::Sender;

use futures::future::Loop;
use futures::Stream;
use futures::{future, Future};

use rand::seq::SliceRandom;
use rand::thread_rng;

use serenity::command;
use serenity::framework::StandardFramework;
use serenity::model::channel::Message;
use serenity::model::gateway::Ready;
use serenity::prelude::*;
use serenity::utils::MessageBuilder;
use serenity::Client;

use std::env;
use std::thread;

use typemap::Key;

use std::process::Command;
use std::process::Stdio;
use std::time::{Duration, Instant};

use tokio::timer::Delay;

use tokio_process::CommandExt;

type ThreadFut = Box<dyn Future<Item = (), Error = ()> + Send + 'static>;

struct FutSender;

impl Key for FutSender {
    type Value = Sender<ThreadFut>;
}

struct Handler;

impl EventHandler for Handler {
    fn ready(&self, _: Context, ready: Ready) {
        info!("Connected; session ID {}", ready.session_id)
    }
}

// We're only ever really queueing commands to be executed,
// so the actual Discord commands are just sending the
// main thread futures to execute asynchronously.
//
// Author's note: I dream of async/await and a more sane
// Serenity API.

struct RunCtx {
    trigger: Message,
    current_str: String,
    current_msg: Message,
    last_edit: Instant,
    start_instant: Instant,
}

impl RunCtx {
    /// The time that must elapse between the same message
    /// being edited.
    const EDIT_DEBOUNCE: Duration = Duration::from_millis(2000);

    /// The maximum length of a message before a new message
    /// **must** be created.
    const MESSAGE_MAX_LEN: usize = 1900;

    /// The length of a message at which `push_line` prefers
    /// to split output to a new message at a newline boundary.
    const MESSAGE_SPLIT_LEN: usize = 1800;

    /// Creates a new reply to edit.
    fn new_response(&mut self) {
        // TODO(scriptis): rustc throws unless we borrow
        // explicitly outside of the closure
        let st = &self.current_str;

        self.current_msg = self
            .trigger
            .reply(
                &MessageBuilder::new()
                    .push_codeblock_safe(st, None)
                    .build(),
            )
            .unwrap();
    }

    /// Immediately pushes the current code block to the
    /// current message.
    pub fn flush(&mut self) {
        // TODO(scriptis): rustc throws unless we borrow
        // explicitly outside of the closure
        let author = &self.trigger.author;
        let st = &self.current_str;

        let _ = self.current_msg.edit(|msg| {
            msg.content(
                MessageBuilder::new()
                    .user(author)
                    .push_codeblock_safe(st, None),
            )
        });

        self.last_edit = Instant::now();
    }

    /// Pushes a line to the output, splitting at a boundary
    /// as necessary.
    pub fn push_line(&mut self, line: &str) {
        for chunk in line
            .chars()
            .collect::<Vec<_>>()
            .chunks(Self::MESSAGE_MAX_LEN)
        {
            let chunk_str = chunk.iter().cloned().collect();

            if chunk.len() + self.current_str.len() > Self::MESSAGE_SPLIT_LEN {
                self.flush();
                self.current_str = chunk_str;
                self.new_response();
            } else {
                self.current_str += "\n";
                self.current_str += &chunk_str;

                if self.last_edit.elapsed() > Self::EDIT_DEBOUNCE {
                    self.flush();
                }
            }
        }
    }
}

/// Generates a future that executes a series of `Command`s
/// in serial, writing their joined output (stderr and stdout)
/// to a `Message`.
macro_rules! command_chain {
    ( $trigger:expr, $( $command:expr ),* ) => {
        {
            use tokio_io::io::lines;
            use std::io::BufReader;

            let trigger: Message = $trigger.clone();

            let fut = future::ok(RunCtx {
                current_msg: trigger.reply("").unwrap(),
                current_str: String::new(),
                trigger,
                last_edit: Instant::now(),
                start_instant: Instant::now(),
            });

            $(
                let mut cmd = $command
                    .stdout(Stdio::piped())
                    .stderr(Stdio::piped())
                    .spawn_async()
                    .unwrap();

                let output =
                    lines(BufReader::new(cmd.stdout().take().unwrap()))
                    .select(lines(BufReader::new(cmd.stderr().take().unwrap())));

                let fut = fut
                    .and_then(move |mut ctx| {
                        ctx.start_instant = Instant::now();

                        output
                        .map_err(|_| ())
                        .fold(ctx, |mut ctx, line| {
                            ctx.push_line(&line);

                            Ok(ctx)
                        })
                    })
                    .join(
                        cmd.map_err(|_| ())
                    )
                    .and_then(|(mut ctx, exit_status)| {
                        let elapsed = Instant::now() - ctx.start_instant;

                        ctx.push_line(&format!(
                            "<acm> Process exited in {}ms with code {}",
                            elapsed.as_millis(),
                            exit_status.code().unwrap_or_default()
                        ));

                        ctx.flush();

                        future::ok(ctx).join(
                            Delay::new(Instant::now() + Duration::from_millis(200)))
                            .map_err(|_| ())
                            .map(|(ctx, _)| ctx)
                    });
            )*

            fut
            .map(|_| ())
            .map_err(|_| ())
        }
    };
}

command!(run(ctx, msg, args) {
    info!("{} ran {}", msg.author.tag(), args.rest());

    let fut = command_chain![
        msg,
        Command::new("docker").args(&["exec", "-i", "acm", "/bin/bash", "-cx", args.rest()])
    ];

    let _ = ctx.data.lock().get::<FutSender>().unwrap().send(Box::new(fut));
});

// For some reason, when spawned as a child process, the Docker
// CLI doesn't like to wait for its job to finish beyond sending
// the request to the daemon, so we're abusing `sh` here to get
// around that.
// 
// Yes, this ruins the point of having gone through the agony
// of creating `command_chain!`.

const RESTART_SH: &str = "\
docker kill acm
docker restart acm
";

command!(restart(ctx, msg) {
    info!("{} restarted the machine", msg.author.tag());

    let fut = command_chain![
        msg,
        Command::new("sh").args(&["-c", RESTART_SH])
    ];

    let _ = ctx.data.lock().get::<FutSender>().unwrap().send(Box::new(fut));
});

const RELOAD_SH: &str = "\
docker kill acm
docker rm acm
docker run -d --name acm --hostname stopallthehacks centos sleep 31d
";

command!(reload(ctx, msg) {
    info!("{} reloaded the machine image", msg.author.tag());

    let fut = command_chain![
        msg,
        Command::new("sh").args(&["-c", RELOAD_SH])
    ];

    let _ = ctx.data.lock().get::<FutSender>().unwrap().send(Box::new(fut));
});

const HELP: &str = include_str!("../USAGE.md");

command!(help(_ctx, msg) {
    let _ = msg.channel_id.send_message(|response| {
        response.content(
            MessageBuilder::new()
            .user(&msg.author)
            .push_codeblock_safe(HELP, Some("md"))
        )
    });
});

command!(banana(_ctx, msg) {
    let _ = msg.channel_id.send_message(|message|
        message.embed(|embed|
            embed.author(|author|
                author
                .name("Dr. Ali")
            )
            .description(
                &[
                    "No banana for you!",
                    "Yes, a banana for you!"
                ]
                .choose(&mut thread_rng())
                .unwrap()
            )
            .color((255, 208, 70))
        )
    );
});

fn main() {
    let _ = Command::new("dockerd").spawn();

    env_logger::init();
    openssl_probe::init_ssl_cert_env_vars();

    let token = env::var("DISCORD_TOKEN").expect("DISCORD_TOKEN must be provided");

    let mut client = Client::new(&token, Handler).expect("Bad client configuration");

    client.with_framework(
        StandardFramework::new()
            .configure(|c| c.prefix("$"))
            .command("run", |c| c.cmd(run))
            .command("restart", |c| c.cmd(restart))
            .command("reload", |c| c.cmd(reload))
            .command("banana", |c| c.cmd(banana))
            .command("help", |c| c.cmd(help)),
    );

    let (sender, receiver) = crossbeam_channel::unbounded();

    client.data.lock().insert::<FutSender>(sender);

    thread::spawn(move || {
        client.start().expect("Could not start client");
    });

    tokio::run(future::loop_fn(receiver, |r| {
        for fut in r.try_iter() {
            tokio::spawn(fut);
        }

        Ok(Loop::Continue(r))
    }));
}
