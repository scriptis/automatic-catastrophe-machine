# The Automatic Catastrophe Machine (ACM)

It's like a regular Linux machine, but worse.

Command reference:
- $help - Print this message
- $run <script> - Pipe <script> to /bin/bash as root
- $restart - Kill and restart the machine
- $reload - Reinstall the entire machine
- $banana - It's a secret to everybody

Anything goes, but please avoid doing anything obviously
stupid :).

Sources available on [GitLab][git].

[git] https://gitlab.com/scriptis/automatic-catastrophe-machine
