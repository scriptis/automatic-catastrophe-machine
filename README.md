# The Automatic Catastrophe Machine (ACM)

Stupid Discord bot that runs arbitrary commands as root in a
Docker image, without any verification, or anything, really.

Yes, it's a terrible idea.

No, I don't care.

It's a bit of a mess, but really, let's be honest here,
the entire thing's just one big hack to begin with.

## Building
You only need [Docker](https://www.docker.com/). Consult your distribution
or download it directly from the website.

Then, run `docker build . -t acm --no-cache`.

The ACM is built against `rust-musl-builder`, so resultant
binaries should work properly on any Linux-based platform.
Of course, you should only actually run the binary in the
Docker image that the provided `Dockerfile` creates, for
the sake of *some* security.

If you plan to contribute to this project, you really only
need Rust. You can download it [here](https://rustup.rs/).
