FROM ekidd/rust-musl-builder AS builder

ADD . ./

RUN sudo chown -R rust:rust /home/rust

RUN cargo build --release

FROM docker:stable-dind

COPY --from=builder \
	/home/rust/src/target/x86_64-unknown-linux-musl/release/automatic-catastrophe-machine \
	/usr/local/bin

ENV RUST_LOG=info

CMD /usr/local/bin/automatic-catastrophe-machine
